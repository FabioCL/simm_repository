from flask import Flask
from views import *
from flask_mail import Mail

app = Flask(__name__)
application = app
mail = Mail(app)

app.register_blueprint(main_site_bp)
