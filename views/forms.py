from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Length, EqualTo, Email
from wtforms.widgets import TextArea


class ContactForm(Form):
    full_name = StringField(
        'Full Name',
        validators=[DataRequired(), Length(min=5, max=50)]
    )
    email = StringField(
        'Email',
        validators=[DataRequired(), Email(), Length(min=6, max=40)]
    )
    password = PasswordField(
        'Password',
        validators=[DataRequired(), Length(min=6, max=40)])
    details = StringField(
        'Details',
        validators=[DataRequired(), Length(max=500)],
        widget=TextArea()
    )