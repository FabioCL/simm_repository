from flask import Blueprint, render_template, redirect, request, url_for, flash
# from emails import send_email


from forms import ContactForm

main_site_bp = Blueprint('main_site', __name__)


@main_site_bp.route('/')
def home():
    return render_template('index.jinja2')


@main_site_bp.route('/about_us')
def about_us():
    return render_template('about_us.jinja2')


@main_site_bp.route('/services')
def services():
    return render_template('services.jinja2')


@main_site_bp.route('/portfolio')
def portfolio():
    return render_template('portfolio.jinja2')


@main_site_bp.route('/pricing')
def pricing():
    return render_template('pricing.jinja2')


@main_site_bp.route('/contact_us', methods=['GET', 'POST'])
def contact_us():
    user_data = []

    error = None
    form = ContactForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            reply_to = request.form.get('email')
            message = request.form.get('message')
            send_email(message, reply_to)
        return render_template('my_form.html')

    return render_template('contact_us.jinja2', **locals())


@main_site_bp.route('/progressive')
def progressive():
    return render_template('progressive.jinja2')


@main_site_bp.route('/digital_marketing')
def digital_marketing():
    return render_template('digital_marketing.jinja2')


@main_site_bp.route('/terms_of_service')
def terms_of_service():
    return render_template('terms_service.jinja2')


@main_site_bp.route('/privacy_policy')
def privacy_policy():
    return render_template('privacy_policy.jinja2')


@main_site_bp.route('/sitemap')
def sitemap():
    return render_template('sitemap.jinja2')


